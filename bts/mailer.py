"""
Mail sender for debbugs control messages

Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import smtplib
from email.mime.text import MIMEText
import email.utils

class BtsMailer:
    _nbsent = 0

    def __init__(self, fake = False):
        self.fake = fake
        if not self.fake:
            self.smtp = smtplib.SMTP()
            self.smtp.connect()

    def BtsMail(self, body):
        # I know this is silly, but latin1 enforces quoted-printable
        # and we need that because of smtp's that assume we use qp,
        # and split our lines.
        msg = MIMEText(body, 'plain', 'iso-8859-1')
        msg['Message-ID'] = email.utils.make_msgid('btslink')
        msg['Date'] = email.utils.formatdate(localtime=True)
        # msg['X-Debbugs-No-Ack'] = 'no-acks'
        return msg

    def sendmail(self, From, To, msg):
        BtsMailer._nbsent += 1

        if self.fake:
            print(("=================================== mail #%i ===================================" % (BtsMailer._nbsent)))
            print("")
            print((msg.as_string()))
            print("")
        else:
            self.smtp.sendmail(From, To, msg.as_string())

    def unlink(self):
        if not self.fake:
            self.smtp.close()

