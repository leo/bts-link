#!/usr/bin/make -f
# Bts-Link make file

# log file will be named log_YYYY-MM-DD
DATE = $(shell date --rfc-3339=date)
LOGFILE = log_$(DATE)
SUMMARYFILE = summary_$(DATE)

export LANG=en_US.UTF-8

doc:
	$(MAKE) -C htdocs

pull:
	# Workrounds, because 'grep' when it prints no row, returns 1...
	git pull 2>&1 | (grep -v "Already up-to-date." || exit 0)

btsnode: pull
	# print stats link, for me to quickly just at it after a run
	echo https://bts-link-team.pages.debian.net/bts-link/stats.html

	# extract data from the bts mirror (we need only summary files)
	# we need only the file for forwarded bugs (forwarded to http links)
	# then we parse data to "<bugs nnn>: <url>", sort and add to index.fwd file
	find /srv/bugs.debian.org/spool/db-h/ -name "*.summary" | xargs grep -H '^Forwarded-To' | grep 'http[s]*://' | sed -e 's~^\(.*\)/\([0-9]*\).summary:Forwarded-To:~\2:~' | sort -n > index.fwd.new

	# replace "real" file
	mv index.fwd.new index.fwd

	# create, if missing, the log directory
	mkdir -p log

	# call btspull with those data
	@DEBEMAIL=debian-bts-link@lists.debian.org timeout 6h ./btspull --short --verbose $(shell cut -d: -f1 index.fwd | tr '\n' ' ') 2>log/$(LOGFILE) 1>log/$(LOGFILE)

	# Update RRD & generate the graphs from it
	./rrd/update_rrd.py log/$(SUMMARYFILE)
	./rrd/graph_rrd.py

	# copy new images to the server hosting bts-link.a.d.o
	chmod a+r /home/btslink/bts-link/htdocs/img/*.png
	rsync -a /home/btslink/bts-link/htdocs/img/*.png morph@people.debian.org:/home/morph/public_html/bts-link/

	# print summary file (quick way to get last run info from cron mail)
	cat log/$(SUMMARYFILE)

tests:
	python3 -m pytest tests/ --verbose --numprocesses=logical

.PHONY: pull btsnode doc tests
