"""
Remote BTS handling

Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import subprocess

from .base import RemoteBts, ParseExn, DupeExn, NoStatusExn, ConnectionFailedExn

from utils import BTSLConfig as Cnf
import platform

if platform.node() == 'sonntag':
    CAPATH = Cnf.get('btsnode', 'capath')
else:
    CAPATH = Cnf.get('local', 'capath')

def wget(uri, cookies=None):
    opts = []
    opts.append("--user-agent='btslink <debian-bts-link@lists.debian.org>'")
    opts.append("--timeout=600")
    opts.append("--no-check-certificate")
    opts.append("-q -o /dev/null")
    opts.append("--header='Accept-Language: en'")
    if cookies:
        opts.append("--load-cookies %s" % cookies)
    opts.append("-O -")
    return subprocess.getoutput("wget %s '%s'" % (' '.join(opts), uri))

def failwith(uri, s, exn = ParseExn):
    raise exn(uri, s)

