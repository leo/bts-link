"""
Allura trackers support (Apache Allura : https://allura.apache.org/)


Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2015 Olivier Berger <olivier.berger@telecom-sudparis.eu>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

from .__init__ import *
import re, urllib.request, urllib.error, urllib.parse, json, string, ssl

class AlluraTrackerData:
    def __init__(self, uri, urlcomponents):
        self.id = urlcomponents or failwith(uri, "Allura: no id : %s" % str(urlcomponents))

        status = None
        resolution = None

        # We access the REST API to fetch JSON description of the bug
        apiurl = "%(uri)s/rest/p/%(project)s/%(type)s/%(id)s/" % urlcomponents

        context = ssl.create_default_context(capath=CAPATH)
        req = urllib.request.Request(apiurl)
        data = json.load(urllib.request.urlopen(req, context=context))
        
        ticketdata = data.get('ticket')
        if data:
            status = ticketdata.get('status').lower()

        self.status = status or failwith(uri, "Allura", exn=NoStatusExn)
        
        # I hope the status are more or less constant among projects, but not sure
        if status == 'closed-duplicate':
            resolution = 'duplicate'
        
        self.resolution = resolution
        
        if self.resolution == 'duplicate':
            raise DupeExn(uri)
        

class RemoteAlluraTracker(RemoteBts):
    '''
    Support for Allura bug trackers
    '''
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, AlluraTrackerData)

    # override base class method to extract the URL components as useful as for _getUri
    def extractBugid(self, uri):
        bugre = re.compile(r"^(?P<uri>.*)/p/(?P<project>[^/]+)/(?P<type>[^/]+)/(?P<id>[0-9]+)/?$")
        res = bugre.match(uri)
        if res:
            return res.groupdict()
        else:
            return None

    # return a meaningful uri
    def _getUri(self, urlcomponents):
        return "%(uri)s/p/%(project)s/%(type)s/%(id)s/" % urlcomponents
    
    def isClosing(self, status, resolution):
        # All status starting by 'closed' are matched by default, unless otherwise specified in the 'closing' conf value
        if not status:
            return False
        else:
            if 'closing' in self._cnf:
                return status in self._cnf['closing']
            else: 
                return status.startswith('closed')
        
    def isWontfix(self, status, resolution):
        # Again I hope the status are more or less constant among projects, but not sure
        return status and status.startswith("closed-wont-fix")

RemoteBts.register('allura', RemoteAlluraTracker)
