"""
Interface to remote bugzillas


Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import re, sys, urllib.request, urllib.parse, urllib.error, time, traceback, os, ssl
from .__init__ import *

from .base import RemoteReport, getActions, warn, die
from bs4 import BeautifulSoup

class BugzillaOldData:
    """ 'Old bugzilla' XML parser """
    
    dupre = re.compile(r'\*\*\* This (?:bug|issue) has been marked as a duplicate of(?: bug)? ([0-9]+) \*\*\*$')

    def __init__(self, uri):
        soup = BeautifulSoup(wget(uri), features="lxml")

        if soup.bugzilla:
            bug         = soup.bugzilla.bug     or failwith(uri, "BugZilla: no bug")
            self.id     = bug.bug_id.string     or failwith(uri, "BugZilla: no bug_id")
            self.status = bug.bug_status.string or failwith(uri, "BugZilla: no bug_status")
        elif soup.issuezilla:
            bug         = soup.issuezilla.issue   or failwith(uri, "IssueZilla: no issue")
            self.id     = bug.issue_id.string     or failwith(uri, "IssueZilla: no issue_id")
            self.status = bug.issue_status.string or failwith(uri, "IssueZilla: no issue_status")
        else:
            failwith(uri, "BugZilla: Invalid XML")

        self.resolution = (bug.resolution and bug.resolution.string) or None
        self.duplicate  = None

        if self.resolution == 'DUPLICATE':
            for t in bug.fetch('thetext')[-1::-1]:
                m   = BugzillaOldData.dupre.search(t.string)
                bug = m and m.group(1)
                if bug:
                    self.duplicate = bug
                    break

            if not self.duplicate:
                failwith(uri, "BugZilla: cannot find duplicate")

class BugzillaData:
    """ Modern bugzilla XML parser """
    
    dupre = re.compile(r'\*\*\* This (?:bug|issue) has been marked as a duplicate of(?: bug)? ([0-9]+) \*\*\*$')

    def __init__(self, bug):

        self.id     = bug.bug_id.string     or failwith("?", "BugZilla: no bug_id")
        self.status = bug.bug_status.string or failwith(self.id, "BugZilla: no bug_status")

        self.resolution = (bug.resolution and bug.resolution.string) or None
        self.duplicate  = None

        if self.resolution == 'DUPLICATE':
            if bug.dup_id:
                self.duplicate = bug.dup_id.string
                return

            for t in bug.fetch('thetext')[-1::-1]:
                m   = BugzillaData.dupre.search(t.string)
                bug = m and m.group(1)
                if bug:
                    self.duplicate = bug
                    break

            if not self.duplicate:
                failwith(self.id, "BugZilla: cannot find duplicate")

class RemoteBugzillaOld(RemoteBts):
    def __init__(self, cnf):
        bugre  = r"^%(uri)s/show_bug.cgi\?id=([0-9]+)$"
        urifmt = "%(uri)s/show_bug.cgi?id=%(id)s"
        RemoteBts.__init__(self, cnf, bugre, urifmt)

    def __getClosingStatus(self):
        if 'closing' in self._cnf:
            return self._cnf['closing']

        try:
            context = ssl.create_default_context(capath=CAPATH)
            config = urllib.request.urlopen(self._cnf['uri'] + '/config.cgi', context=context)
        except (ssl.CertificateError, IOError) as e:
            # let's try not to verify that certificate at all
            context = ssl.create_default_context()
            context.check_hostname = False
            context.verify_mode = ssl.CERT_NONE
            config = urllib.request.urlopen(self._cnf['uri'] + '/config.cgi', context=context)

        config = config.read().decode()
        for l in config.splitlines():
            if l.startswith('var status_closed'):
                s = l[l.find('['):].strip('[] ,;\r\t\n')
                self._cnf['closing'] = [x.strip("' ") for x in s.split(',')]
                return self._cnf['closing']


    def isClosing(self, status, resolution):
        return resolution != 'WONTFIX' and status in self.__getClosingStatus()

    def isWontfix(self, status, resolution):
        return resolution == 'WONTFIX'

    def _getApiUri(self, bugId):
        return "%s/xml.cgi?id=%s" % (self._cnf['uri'], bugId)

    def _getReportData(self, uri):
        id = self.extractBugid(uri)
        if not id: return None

        uri = self._getApiUri(id)
        
        data = BugzillaOldData(uri)
        while data.resolution == 'DUPLICATE':
            uri  = "%s/xml.cgi?id=%s" % (self._cnf['uri'], data.duplicate)
            data = BugzillaOldData(uri)

        return data

class RemoteBugzilla(RemoteBugzillaOld):
    
    api_fields = ["bug_id", "bug_status", "resolution", "dup_id", "long_desc"]
    
    def __init__(self, cnf):
        RemoteBugzillaOld.__init__(self, cnf)

    def _getApiUri(self, bugId):
        qs  = "ctype=xml&field=" + "&field=".join(self.api_fields)
        qs += '&id=%s' % bugId
        return '%s/show_bug.cgi?%s' % (self._cnf['uri'], qs)

    def enqueue(self, btsbug):
        id = self.extractBugid(btsbug.forward)
        if id is None:
            failwith(btsbug.forward, "BugZilla: cannot extract id from fwd: %s" % (btsbug.forward))
        self._queue.append((btsbug, id))

    def processQueue(self):
        res = []
        status = {'A': 0, 'C': 0, 'E': 0, 'I': 0}
        msgs = []
        while self._queue:
            if len(self._queue) > 100:
                subq = self._queue[0:100]
                self._queue = self._queue[100:]
            else:
                subq = self._queue
                self._queue = []

            qs  = "ctype=xml&field=" + "&field=".join(self.api_fields)
            qs += '&id=' + '&id='.join([x[1] for x in subq])
            massuri = "%s/show_bug.cgi?%s" % (self._cnf['uri'], qs)

            assoc = {}
            for v, k in subq: assoc[k] = v

            bugs = BeautifulSoup(wget(massuri), features="xml")
            if not bugs:
                status['E'] += 1
                msgs.append("E: uri=%s, msg=invalid XML?" % massuri)
                continue

            for bug in bugs.find_all('bug'):
                try:
                    data = BugzillaData(bug)
                    if not data: continue

                    rbug = RemoteReport(data, self)

                    status['C'] += 1
                    msgs.append("C: pkg=%s, bug=%s, msg=check completed successfully" % (assoc[rbug.id].package, assoc[rbug.id].id))

                    if rbug.resolution == 'DUPLICATE':
                        self._queue.append((assoc[rbug.id], data.duplicate))
                        continue

                    actions, only_actions = getActions(assoc[rbug.id], rbug)
                    if actions:
                        status['A'] += len(only_actions)
                        for action in only_actions:
                             msgs.append("A: pkg=%s, bug=%s, action=%s" % (assoc[rbug.id].package, assoc[rbug.id].id, action))
                        res.append((assoc[rbug.id], actions))
                except KeyboardInterrupt:
                    die("*** ^C...")
                except ParseExn as e:
                    status['E'] += 1
                    msgs.append("E: uri=%s, msg=%s" % (self._cnf['uri'], str(e)))
                except DupeExn as e:
                    status['E'] += 1
                    msgs.append("E: pkg=%s, bug=%s, msg=does not deal with dupes: [%s]" % (assoc[rbug.id].package, assoc[rbug.id].id, str(e)))
                except:
                    status['E'] += 1
                    i = 0
                    type, value, tb = sys.exc_info()
                    exn = traceback.format_exception_only(type, value)
                    msgs.append("E: uri=%s, msg=exception raised, %s, traceback follows" % (massuri, exn))
                    for file, lineno, _, text in traceback.extract_tb(tb):
                        i += 1
                        msgs.append("  %d. %-70s [%s:%s]" % (i, text, os.path.basename(file), lineno))

            if not self._queue:
                break

            # yes sleep a lot, we go fast
            time.sleep(20)
        return res, status, msgs

RemoteBts.register('bugzilla-old', RemoteBugzillaOld)
RemoteBts.register('bugzilla', RemoteBugzilla)
