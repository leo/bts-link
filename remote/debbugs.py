"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

try:
    from debianbts import debianbts
except:
    # needed to support the version in stretch, 2.6.1
    import debianbts
from .__init__ import *

class DebbugsData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Debbugs: no id")
        domain = 'https://'+uri.split('/')[2]

        # FIXME: different debbugs instances can have different cgi paths
        debianbts._soap_client_kwargs['location'] = domain+'/cgi/soap.cgi'

        bug_status = debianbts.get_status(id)[0]

        self.status = bug_status.pending
        self.resolution = ' '.join(bug_status.tags)

class RemoteDebbugs(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/(?:cgi/bugreport.cgi\?bug=|)([0-9]+)$'
        urifmt = '%(uri)s/%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, DebbugsData)

    def isClosing(self, status, resolution):
        return status in ('done',)

    def isWontfix(self, status, resolution):
        if resolution:
            return 'wontfix' in resolution
        else:
            return False

RemoteBts.register('debbugs', RemoteDebbugs)
