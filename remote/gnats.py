"""
Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright Christoph Berg <myon@debian.org>
Copyright 2006 Sanghyeon Seo <sanxiyn@gmail.com> (author of rt.py, copied by this module)
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.parse, urllib.error, urllib.parse

from bs4 import BeautifulSoup
from .__init__ import *

# <tr><td nowrap><b>State:</b></td>
# <td><tt>open</tt></td></tr>

def parse_table(soup, key):
    cell = soup.firstText(key).findParent('td')
    return cell.findNextSibling('td').first('tt').string

# Cookie: gnatsweb-global=email&cb%40df7cb.de&database&mutt&Submitter-Id&any&columns&Notify-List%20Category%20Synopsis%20Confidential%20Severity%20Priority%20Responsible%20State%20Keywords%20Date-Required%20Class%20Submitter-Id%20Arrival-Date%20Closed-Date%20Last-Modified%20Originator%20Release; gnatsweb-db-mutt=password&g%60vft&user&guest

class GnatsData:
    def __init__(self, uri, id):
        soup = BeautifulSoup(wget(uri, "cookies.txt"), features="lxml")

        self.id = id or failwith(uri, "Gnats: no id")
        self.status = parse_table(soup, 'State:') or failwith(uri, "Gnats", exn=NoStatusExn)
        self.resolution = None

class RemoteGnats(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/([0-9]+)$'
        urifmt = '%(uri)s/%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, GnatsData)

    def isClosing(self, status, resolution):
        return status in ('closed',)

    def isWontfix(self, status, resolution):
        return status in ('suspended',)

RemoteBts.register('gnats', RemoteGnats)
