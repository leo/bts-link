"""
@see http://code.google.com/p/support/wiki/IssueTracker


Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.parse, urllib.error, urllib.parse, re, ssl

from bs4 import BeautifulSoup
from .__init__ import *

def parse_table(soup):

    # get the table with metainfo, identified by id=issuemeta
    cell = soup.first('td', attrs={'id' : 'issuemeta'})
    # get the first td, the span in it, then its string,
    # stripping additional newlines
    return cell.first('td').first('span').string.strip()

class GoogleCodeData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "GoogleCode: no id")

        context = ssl.create_default_context(capath=CAPATH)
        soup = BeautifulSoup(urllib.request.urlopen(uri, context=context), features="lxml")

        self.status = parse_table(soup) or failwith(uri, "GoogleCode", exn=NoStatusExn)
        self.resolution = None

        if self.status == 'Duplicate':
            raise DupeExn(uri)

class RemoteGoogleCode(RemoteBts):
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, GoogleCodeData)

    # override base class method to extract the ID as useful as for _getUri
    def extractBugid(self, uri):
        if 'crbug' in uri:
            bugre = re.compile(r"http://crbug.com/(?P<id>[0-9]+)$")
            ret = bugre.match(uri).groupdict()
            return {'project': 'chromium', 'id': ret['id']}
        else:
            bugre = re.compile(r"https?://code.google.com/p/(?P<project>.*)/issues/detail\?id=(?P<id>[0-9]+)$")
            return bugre.match(uri).groupdict()

    # return a meaningful uri
    def _getUri(self, bugId):
        return "http://code.google.com/p/%(project)s/issues/detail?id=%(id)s" % bugId

    def isClosing(self, status, resolution):
        return status in ('Fixed', 'Verified', 'Archived')

    def isWontfix(self, status, resolution):
        return status in ('WontFix', 'Invalid')

RemoteBts.register('googlecode', RemoteGoogleCode)
