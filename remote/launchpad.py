"""
Copyright 2008 Jelmer Vernooij <jelmer@samba.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.parse, urllib.error, urllib.parse, re, ssl

from .__init__ import *
import email


class LaunchpadTask:
    # something like 'product' or 'product (distro)'
    productre = re.compile(r'(\S+)(?: \(([^)]+)\))?')

    def __init__(self, msg):
        product = msg["task"]
        #print product
        m = LaunchpadTask.productre.search(product)
        #print m.groups()
        distro = m and m.group(2)
        product = m and m.group(1)
        
        #print "coin : ", product, distro

        self.product = product
        self.distro = distro
        self.status = msg["status"]
        self.assignee = msg["assignee"]
        self.reporter = msg["reporter"]


class LaunchpadBug:
    def __init__(self, text):
        bug = email.message_from_string(text)
        dupestr = bug.get("duplicate-of", None)
        if dupestr is None or dupestr == "":
            self.duplicate_of = None
        else:
            self.duplicate_of = int(dupestr)
        self.id = int(bug["bug"])
        self.tasks = {}
        self.title = bug["title"]
        self.reporter = bug["reporter"]

        # Find the task related to the product we're dealing with at the moment
        # the +text output is a series of email-like header sections one after the other so
        # once email.message_from_string() parsed the first set of headers, considers all
        # those remaining as payload; so keep iterating over it until we parse all of them
        task = email.message_from_string(bug.get_payload())
        while "task" in task:
            lptask = LaunchpadTask(task)
            self.tasks[task["task"]] = lptask 
            task = email.message_from_string(task.get_payload())


class LaunchpadData:
    def __init__(self, uri, id=None):
        context = ssl.create_default_context(capath=CAPATH)
        text = urllib.request.urlopen(urllib.parse.urljoin(uri+"/", "+text"), context=context).read().decode()
        # Two forms of URLs supported:
        if uri.split("/")[-2] == "+bug":
            # https://bugs.launchpad.net/bzr/+bug/XXXXXX
            product = uri.split("/")[-3]
        else:
            # https://bugs.launchpad.net/bugs/XXXXXX
            product = None
        self.resolution = None

        lp_bug = LaunchpadBug(text)
        if id is not None and int(id) != lp_bug.id:
            raise ParseExn(uri, "Expected id %s, got %d" % (id, lp_bug.id))
        self.id = lp_bug.id
        # if we were to follow redirects to record new URI, we would :
        #self.uri = text.geturl()

        if lp_bug.duplicate_of is not None:
            dupe_uri = "%s/%d" % ("/".join(uri.split("/")[:-1]), 
                                  lp_bug.duplicate_of)
            self.duplicate = LaunchpadData(dupe_uri)
        else:
            self.duplicate = None

        if product is None:
            # no product was present in URL
            if len(lp_bug.tasks) != 1:
                # try to match a product that ends with 'Debian'
                for task in lp_bug.tasks:
                    if task.endswith('(Debian)'):
                        product = task
                        break
                if product is None:
                    raise ParseExn(uri, "No product specified but bug affects multiple products")
            else:
                # as there's only one task, it's obviously that one
                product = list(lp_bug.tasks.keys())[0]
        else :
            # product is present in URL, so need some more checks
            #print lp_bug.tasks.keys()
            if product not in lp_bug.tasks:
                # the name of the package may be tagged with '(Debian)', so let's check for that too
                if product + ' (Debian)' in lp_bug.tasks:
                    product = product + ' (Debian)'
                # if exactly the product was not in tasks, there's a special case when only one task
                elif len(lp_bug.tasks) > 1 :
                    raise ParseExn(uri, "Product %s not found in many products affected" % product)
                else :
                    # check that the only task's product is that one 
                    taskname = list(lp_bug.tasks.keys())[0]
                    if product != lp_bug.tasks[taskname].product :
                        raise ParseExn(uri, "Product %s not recognised" % product)
                    product = taskname

        self.status = lp_bug.tasks[product].status


class RemoteLaunchpad(RemoteBts):
    def __init__(self, cnf):
        bugre  = r"^%(uri)s/(?:.*/)?(?:\+bug|bugs)/([0-9]+)$"
        urifmt = "%(uri)s/bugs/%(id)s"
        RemoteBts.__init__(self, cnf, bugre, urifmt, LaunchpadData)

    def isClosing(self, status, resolution):
        return status == "Fix-Released" or status == "Fix-Committed"

    def isWontfix(self, status, resolution):
        return status == "Won-t-Fix" or status == "Invalid"

RemoteBts.register('launchpad', RemoteLaunchpad)

