"""
Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.parse, urllib.error, urllib.parse, re

from bs4 import BeautifulSoup
from .__init__ import *
from .base import maketoken

# TODO: potentially we can remove these 2 lines and parse_table(), maybe find_dupe() too
#status_re = re.compile(r'^\s*Status\s*$')
#resolution_re = re.compile(r'^\s*Resolution\s*$')

#def parse_table(soup, key):
    #cell = soup.firstText(key).findParent('td') or None
    #return cell and maketoken(cell.findNextSibling('td').string.strip())
    #cell = soup.find_all(attrs={'class': key})[0].find_parent().text.strip()
    #return cell

def find_dupe(soup):
    cell = soup.firstText('duplicate of').findParent('td')
    cell = cell.findNextSibling('td')
    return cell.a.string

class MantisData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Mantis: no id")

        soup = BeautifulSoup(wget(uri), features="lxml")

        self.status     = soup.find_all(attrs={'class': "fa-status-box"})[0].find_parent().text.strip() or failwith(uri, "Mantis", exn=NoStatusExn)
        self.resolution = soup.find_all(attrs={'class': "bug-resolution"})[1].text

        if self.resolution == 'duplicate':
            self.duplicate = find_dupe(soup) or failwith(uri, "Mantis: cannot find duplicate")

class RemoteMantis(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/(?:view.php\?id|bug_view_(?:advanced_)?page.php\?bug_id)=([0-9]+)$'
        urifmt = '%(uri)s/view.php?id=%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, MantisData)

    def isClosing(self, status, resolution):
        return status in ('resolved', 'closed') and not self.isWontfix(status, resolution)

    def isWontfix(self, status, resolution):
        return resolution in ('won-t-fix', 'not-fixable')


    def _getReportData(self, uri):
        id = self.extractBugid(uri)
        if not id: return None

        data = MantisData(self.getUri(id), id)

        while data.resolution == 'duplicate':
            data = MantisData(self.getUri(data.duplicate), data.duplicate)

        return data

RemoteBts.register('mantis', RemoteMantis)
