"""
This file will contain misc project issue tracking systems
not abstract enough to warrant a separate file


Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""


### PHP ###
#
# TODO: support PEAR
#       PEAR xml link: http://pear.php.net/bugs/rss/bug.php?format=xml&id=2415

import urllib.request, urllib.parse, urllib.error#, urlparse
import ssl

#from BeautifulSoup import BeautifulSoup
from .__init__ import *

from xml.etree import ElementTree as ET


class PhpData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Php: no id")

        context = ssl.create_default_context(capath=CAPATH)

        # http://www.learningpython.com/2008/05/07/elegant-xml-parsing-using-the-elementtree-module/
        if 'php.net' in uri:
            feed = urllib.request.urlopen(uri.replace(self.id, '').replace('bug.php?id=', '') + "/rss/bug.php?format=xml&id=" + self.id, context=context)
        elif 'bugs.mysql.com' in uri:
            feed = urllib.request.urlopen(uri.replace(self.id, '').replace('bug.php?id=', '') + "/bug.php?format=xml&id=" + self.id, context=context)
        tree = ET.parse(feed)

        # find the element 'status', and get its text
        self.status = tree.find('status').text
        self.resolution = None

        if self.status == 'Duplicate':
            raise DupeExn(uri)

class RemotePhp(RemoteBts):
# lists of status-es
# http://cvs.php.net/viewvc.cgi/php-bugs-web/include/functions.inc?view=markup#l82
    def __init__(self, cnf):

        urifmt = '%(uri)s%(id)s'

        RemoteBts.__init__(self, cnf, cnf['uri'], urifmt, PhpData)

    def isClosing(self, status, resolution):
        return status in ('Closed',)

    def isWontfix(self, status, resolution):
        return status in ('Wont-fix', 'Bogus')

RemoteBts.register('php', RemotePhp)
