"""
Copyright 2006 Sanghyeon Seo <sanxiyn@gmail.com>
Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.request, urllib.parse, urllib.error, urllib.parse, ssl

from bs4 import BeautifulSoup
from .__init__ import *
from .base import maketoken

def parse_table(soup, key):
    cell  = soup.find_all("span", string=key)[0].find_parent("td")
    return maketoken(cell.find_next_sibling().string)

class SavaneData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "Savane: no id")

        context = ssl.create_default_context(capath=CAPATH)
        soup = BeautifulSoup(urllib.request.urlopen(uri, context=context), features="lxml")

        self.status     = parse_table(soup, 'Open/Closed:') or failwith(uri, "Savane", exn=NoStatusExn)
        self.resolution = parse_table(soup, 'Status:')

        if self.resolution == 'Duplicate':
            raise DupeExn(uri)

class RemoteSavane(RemoteBts):
    def __init__(self, cnf):
        bugre  = r'^%(uri)s/\?func=detailitem&item_id=([0-9]+)$'
        urifmt = '%(uri)s/?func=detailitem&item_id=%(id)s'
        RemoteBts.__init__(self, cnf, bugre, urifmt, SavaneData)

    def isClosing(self, status, resolution):
        return status == 'Closed' and resolution != 'Wont-Fix'

    def isWontfix(self, status, resolution):
        return resolution in ('Wont-Fix', 'Invalid')

RemoteBts.register('savane', RemoteSavane)

