"""
Copyright 2006-2009 Pierre Habouzit <madcoder@debian.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import urllib.parse, subprocess

from bs4 import BeautifulSoup
from .__init__ import *
from .base import maketoken

def get_value(table, key):
    td = table.firstText(key).findParent('td')
    return td.contents[-1].string.strip()

class SourceForgeData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "SourceForge: no id")

        soup  = BeautifulSoup(wget(uri), features="lxml")
        table = soup.first('table', {'cellpadding': '0', 'width': '100%'})
 
        self.status     = get_value(soup, 'Status: ') or failwith(uri, "SourceForge", exn=NoStatusExn)
        self.resolution = get_value(soup, 'Resolution: ') or None

        if self.resolution == 'Duplicate':
            raise DupeExn(uri)

def get_value2(table, key):
    value = table.find_all(string=key)
    if value:
        return value[0].find_parent().find_next("span").string.strip()
    return None


class SourceForgeTrackerTwoData:
    def __init__(self, uri, id):
        self.id = id or failwith(uri, "SourceForge: no id")

        soup  = BeautifulSoup(wget(uri), features="lxml")
        #print soup.prettify()

        self.status     = get_value2(soup, 'Status: ') or failwith(uri, "SourceForge", exn=NoStatusExn)
        self.resolution = get_value2(soup, 'Resolution:') or None

        if self.resolution == 'Duplicate':
            raise DupeExn(uri)
    

class RemoteSourceForge(RemoteBts):
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, SourceForgeData)

    def _getUri(self, bugId):
        k = ['atid', 'aid', 'group_id']
        qs = []
        for x in k :
            if x in bugId :
                qs.append('%s=%s' % (x, bugId[x]))
        return "http://sourceforge.net/tracker/?func=detail&" + '&'.join(qs)

    def extractBugid(self, uri):
        query = urllib.parse.parse_qs(urllib.parse.urlparse(uri)[4])
        id = {}
        if not 'aid' in query: failwith(uri, "SourceForge: aid key is missing")
        for k in 'atid', 'aid', 'group_id':
            if k in query :
                id[k] = query[k][0]
        return id

    def isClosing(self, status, resolution):
        return status == 'Closed' and resolution != 'Wont-Fix'

    def isWontfix(self, status, resolution):
        return resolution == 'Wont-Fix'

class RemoteSourceForgeTrackerTwo(RemoteSourceForge):
    def __init__(self, cnf):
        RemoteBts.__init__(self, cnf, None, None, SourceForgeTrackerTwoData)


# TODO: remove `sourceforge` and leave only `sourceforge2`?
RemoteBts.register('sourceforge', RemoteSourceForge)
RemoteBts.register('sourceforge2', RemoteSourceForgeTrackerTwo)
