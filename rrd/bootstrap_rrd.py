#!/usr/bin/python3
"""
Initialize the RRD db from the summary files of previous bts-link executions

Docs used:
http://oss.oetiker.ch/rrdtool/doc/rrdcreate.en.html
http://oss.oetiker.ch/rrdtool/doc/rrdupdate.en.html


Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import sys
import yaml
import datetime
from collections import defaultdict
import rrdtool
import os.path

if len(sys.argv) == 1:
    print(("Usage: " + sys.argv[0] + " <list of summary files>"))
    sys.exit(1)

basedir = os.path.dirname(os.path.abspath(__file__))
rrdfile = os.path.join(basedir, 'bts-link.rrd')

r = defaultdict(lambda: defaultdict(dict))
tags = set(['elapsed_time', ])

for summaryfile in sorted(sys.argv[1:]):
    with open(summaryfile) as f:
        summary = yaml.load(f, Loader=yaml.FullLoader)
        # rrd wants epoch as timestamps
        summary_epoch = summary['Execution complete'].strftime('%s')
        r[summary_epoch]['elapsed_time'] = int(summary['Elapsed time'])
        for tag in summary['Tags summary']:
            tags.add(tag)
            r[summary_epoch][tag] = summary['Tags summary'][tag]

# http://oss.oetiker.ch/rrdtool/doc/rrdcreate.en.html
rrdtool.create(rrdfile,
    '--start', '1233446400',  # 2009-02-01 00:00:00
    '--step', '86400',
    # datasource definition
    ['DS:%s:GAUGE:604800:0:U' % ds for ds in sorted(tags)],
    'RRA:LAST:0.1:1:5475'
)

for ts in sorted(r.keys()):
    rrdtool.update(rrdfile,
        '-t', ':'.join(sorted(r[ts].keys())),
        '%s:%s' % (ts, ':'.join(str(r[ts][k]) for k in sorted(r[ts].keys())))
        )