#!/usr/bin/python3
"""
Generate historical graphs from data stored in RRD

Docs used:
http://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html
http://oss.oetiker.ch/rrdtool/doc/rrdgraph_data.en.html
http://oss.oetiker.ch/rrdtool/doc/rrdgraph_graph.en.html
http://oss.oetiker.ch/rrdtool/tut/rrdtutorial.en.html#ITime_to_create_some_graphics


Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""

import rrdtool
import os.path
import os
import itertools

history = ['3 months', '6 months',
           '1 year', '5 years', '10 years',
          ]

tags = {
        'A': 'actions to perform',
        'C': 'checks successfully done',
        'D': 'bugs done',
        'E': 'errors',
        'I': 'no status',
        'M': 'mails sent',
        'N': 'not a bts/ignored',
        'S': 'SMTP errors (sending mails)',
        'T': 'total bugs count',
        'U': 'unmatched/unconfigured bts',
        'X': 'bugs not existing',
}
tags['elapsed_time'] = 'Elapsed time'

overview = ['A', 'C', 'E', 'T', 'U', 'elapsed_time']
colors = dict(list(zip(overview,
                  ['00CC00', '009900', 'CC0000', '0066CC', 'FF9900', 'CCCC00']
             )))
max_ov_tag_length = max(len(tags[x]) for x in overview)

basedir = os.path.dirname(os.path.abspath(__file__))
imgdir = os.path.join(basedir, '../htdocs/img')
rrdfile = os.path.join(basedir, 'bts-link.rrd')

for ds in sorted(tags.keys()):
    for start in history:
        outfile = os.path.join(imgdir, ds + '_' + start.replace(' ', '') + '.png')
        rrdtool.graph(outfile,
            "--start", "-%s" % start,
            "--width", "600",
            "--height", "250",
            '--title', '%s in the last %s' % (tags[ds], start),
            "DEF:%s=%s:%s:LAST" % (ds, rrdfile, ds),
            "LINE:%s#0000FF" % ds,
        )

outfile = os.path.join(imgdir, 'overview' + '.png' )
rrdtool.graph(outfile,
              "--start", "-1 year",
              "--width", "600",
              "--height", "250",
              "--logarithmic",
              '--title', 'bts-link last year executions overview',
              # graph() expect to receive the directives one-by-one as arguments, but
              # since we want to generate them dinamically, we need to force the expansions
              # of the list with *
              *(["DEF:%s=%s:%s:LAST" % (x, rrdfile, x) for x in overview]
              +
              # we can define alle the DEF at once, but LINE and GPRINT for each
              # datasource need to be one next to each other
              # since we generate a list of tuples, we unpack them using chain() which
              # returns a generator, so we list() it
              # we pad the Current string taking the max length of a tag in the overview
              # and then removing the current string length
              list(itertools.chain.from_iterable(
                  ('LINE:%s#%s:%s' % (x, colors[x], tags[x]),
                   'GPRINT:%s:LAST:%sCurrent\:%%8.2lf %%s' % (x, ' '*(max_ov_tag_length - len(tags[x]))), 'COMMENT:\\n') for x in overview
                  ))
              )
        )
