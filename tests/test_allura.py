"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import re

from remote import allura


def test_alluradata():
    bug = allura.AlluraTrackerData("https://sourceforge.net/p/iperf2/tickets/222/", {'id': '222', 'project': 'iperf2', 'type': 'tickets', 'uri': 'https://sourceforge.net'})
    assert bug.status == 'closed'


def test_remoteallura():
    remote = allura.RemoteAlluraTracker({'uri': 'https://sourceforge.net/p', 'uri-re': re.compile('https?://sourceforge.net/p/(?!enigmail).*/[^/]+/([0-9]+)/?'), 'closing': ['fixed'], 'type': 'allura'})
    assert remote.extractBugid("https://sourceforge.net/p/iperf2/tickets/222/")['id'] == "222"
