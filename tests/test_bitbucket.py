"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
from remote import bitbucket
import re


def test_bitbucketdata():
    bug = bitbucket.BitbucketData("https://bitbucket.org/tildeslash/monit/issues/291", {'id': 291, 'user': 'tildeslash', 'project': 'monit'})
    assert bug.status == 'resolved'


def test_remotebitbucket():
    remote = bitbucket.RemoteBitbucket({'uri': 'http://bitbucket.org', 'uri-re': re.compile('https?://(?:www.)?bitbucket.org/'), 'type': 'bitbucket'})
    assert remote.extractBugid("https://bitbucket.org/tildeslash/monit/issues/291")['id'] == '291'
