"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import bs4
import pytest

from remote import bugzilla


def test_bugzilladata():
    _data = """<bug>
<bug_id>256266</bug_id>
<bug_status>RESOLVED</bug_status>
<resolution>CLOSED</resolution>
<thetext>test</thetext>
</bug>
    """
    bug = bugzilla.BugzillaData(bs4.BeautifulSoup(_data, "xml"))
    assert bug.status == 'RESOLVED'


def test_remotebugzilla():
    remote = bugzilla.RemoteBugzilla({'uri': 'https://bugs.webkit.org', 'type': 'bugzilla'})
    assert remote.extractBugid("https://bugs.webkit.org/show_bug.cgi?id=242883") == '242883'


@pytest.mark.xfail(reason="no active remotes associated with this remote, remove?")
def test_bugzillaold():
    assert False