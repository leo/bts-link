"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
from remote import github

def test_githubdata():
    bug = github.GithubData("https://github.com/etcd-io/etcd/pull/15656", 15656)
    assert bug.status == 'closed'


def test_remotegithub():
    remote = github.RemoteGithub({'uri': 'https://github.com', 'type': 'github'})
    assert remote.extractBugid("https://github.com/etcd-io/etcd/pull/15656")['id'] == "15656"
