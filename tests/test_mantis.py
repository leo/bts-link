"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
from remote import mantis


def test_mantisdata():
    bug = mantis.MantisData("https://bugs.scribus.net/view.php?id=11420", 800778)
    assert bug.status == 'confirmed'


def test_remotemandis():
    remote = mantis.RemoteMantis({'uri': 'http://bugs.scribus.net', 'type': 'mantis'})
    assert remote.extractBugid("http://bugs.scribus.net/view.php?id=11420") == "11420"
