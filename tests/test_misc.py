"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import re

from remote import misc


def test_misc_phpdata():
    bug = misc.PhpData("https://pear.php.net/bugs/bug.php?id=23782", "23782")
    assert bug.status == 'Closed'

def test_misc_remotephp():
    remote = misc.RemotePhp({'uri': 'http://pear.php.net/rss/bug.php?format=xml&id=', 'uri-re': re.compile('https?://pear.php.net/bugs/(?:bug.php\\?id=|)?([0-9]+)'), 'type': 'php'})
    assert remote.extractBugid("https://pear.php.net/bugs/bug.php?id=23782") == "23782"

def test_misc_phpdata_mysql():
    bug = misc.PhpData("https://bugs.mysql.com/bug.php?id=98839", "98839")
    assert bug.status == 'Closed'

def test_misc_remotephp_mysql():
    remote = misc.RemotePhp({'uri': 'http://bugs.mysql.com/bug.php?format=xml&id=', 'uri-re': re.compile('https?://bugs.mysql.com/(?:bug.php\\?id=|)?([0-9]+)'), 'type': 'php'})
    assert remote.extractBugid("https://bugs.mysql.com/bug.php?id=98839") == "98839"
