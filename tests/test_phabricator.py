"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
from remote import phabricator

def test_phabricatordata():
    bug = phabricator.PhabricatorData("https://dev.gnupg.org/T5935", "T5935")
    assert bug.status == 'Closed'


def test_remotephabricator():
    remote = phabricator.RemotePhabricator({'uri': 'https://dev.gnupg.org', 'type': 'phabricator'})
    assert remote.extractBugid("https://dev.gnupg.org/T5935") == "T5935"
