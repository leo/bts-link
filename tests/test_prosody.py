"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import pytest
import re

from remote import prosody
from remote.base import DupeExn


@pytest.mark.parametrize(
    "url,status,resolution", [
        ("https://issues.prosody.im/1815", "open",   "new"),
        ("https://issues.prosody.im/1346", "open",   "needinfo"),
        ("https://issues.prosody.im/300",  "open",   "accepted"),
        ("https://issues.prosody.im/1604", "open",   "blocked"),
        ("https://issues.prosody.im/881",  "open",   "started"),
        ("https://issues.prosody.im/1182", "closed", "fixed"),
        ("https://issues.prosody.im/23",   "closed", "verified"),
        ("https://issues.prosody.im/231",  "closed", "invalid"),
        ("https://issues.prosody.im/20",   "closed", "wontfix"),
        ("https://issues.prosody.im/394",  "closed", "cantfix"),
        ("https://issues.prosody.im/354",  "closed", "cantreproduce"),
        ("https://issues.prosody.im/280",  "closed", "done"),
    ]
)
def test_prosodydata(url, status, resolution):
    bug = prosody.ProsodyIssueData(url, 666)
    assert bug.status == status


def test_prosodydata_duplicate():
    with pytest.raises(DupeExn):
        bug = prosody.ProsodyIssueData("https://issues.prosody.im/133", 666)
        assert bug.status == "closed"

def test_remoteprosody():
    remote = prosody.RemoteProsodyIssues({'uri': 'https://issues.prosody.im', 'uri-re': re.compile('https://issues\\.prosody\\.im/([0-9]+)'), 'type': 'prosody'})
    assert remote.extractBugid("https://issues.prosody.im/1147") == "1147"
