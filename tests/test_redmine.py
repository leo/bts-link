"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import pytest
import re

from remote import redmine


def test_redminedata():
    bug = redmine.RedmineData("https://redmine.audacious-media-player.org/issues/78", 700804)
    assert bug.status == 'Closed'


def test_remoteredmine():
    remote = redmine.RemoteRedmine({'uri': 'http://redmine.audacious-media-player.org', 'uri-re': re.compile('https?://redmine.audacious-media-player.org/issues(?:/show)?/([0-9]+)'), 'type': 'redmine'})
    assert remote.extractBugid("https://redmine.audacious-media-player.org/issues/78") == '78'
    #assert remote.isClosing("https://redmine.audacious-media-player.org/issues/78")