"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import re

from remote import rt


def test_rt4data():
    bug = rt.RT4Data("http://krbdev.mit.edu/rt/Ticket/Display.html?id=8763", 917387)
    assert bug.status == 'resolved'

def test_remotert4():
    remote = rt.RemoteRT4({'uri': 'http://krbdev.mit.edu/rt', 'uri-re': re.compile('https?://krbdev.mit.edu/rt/(?:Ticket/Display|Public/Bug/Display|NoAuth/Bug).html\\?id=([0-9]+)'), 'type': 'rt4'})
    assert remote.extractBugid("http://krbdev.mit.edu/rt/Ticket/Display.html?id=8763") == '8763'

def test_rt5data():
    bug = rt.RT5Data("https://rt.cpan.org/Ticket/Display.html?id=134117", 1026046)
    assert bug.status == 'open'

def test_remotert5():
    remote = rt.RemoteRT5({'uri': 'http://rt.cpan.org', 'uri-re': re.compile('https?://rt.cpan.org/(?:Ticket/Display|Public/Bug/Display|NoAuth/Bug).html\\?id=([0-9]+)'), 'type': 'rt5'})
    assert remote.extractBugid("https://rt.cpan.org/Ticket/Display.html?id=134117") == '134117'
