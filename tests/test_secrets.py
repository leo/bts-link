"""
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
from remote import secrets

def test_secrets():
    assert secrets.SECRETS


def test_secrets_github():
    assert 'github' in secrets.SECRETS
    assert secrets.SECRETS['github'].startswith('ghp_')


def test_secrets_jira():
    assert 'jira' in secrets.SECRETS