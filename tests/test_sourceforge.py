"""
Copyright 2008 Jelmer Vernooij <jelmer@samba.org>
Copyright 2009-2021 Sandro Tosi <morph@debian.org>

License: BSD-3-Clause, see LICENSE for the full text
"""
import pytest, re

from remote import sourceforge


@pytest.mark.offline
def test_getUri():
    assert sourceforge.RemoteSourceForge(None)._getUri({"atid": "42", "aid": "4", "group_id": "1337"}) == "http://sourceforge.net/tracker/?func=detail&atid=42&aid=4&group_id=1337"


@pytest.mark.offline
def test_extractBugid():
    assert {"atid": "42", "aid": "4", "group_id": "1337"} == sourceforge.RemoteSourceForge(None).extractBugid("http://sourceforge.net/tracker/?func=detail&atid=42&aid=4&group_id=1337")

def test_sourceforgedata():
    bug = sourceforge.SourceForgeTrackerTwoData("http://sourceforge.net/support/tracker.php?aid=3555888", 905911)
    assert bug.status == 'open'


def test_remotesourceforge():
    remote = sourceforge.RemoteSourceForge({'uri': 'http://sf.net/tracker', 'uri-re': re.compile('https?://(?:sf|sourceforge).net/(?:support/tracker.php|tracker/)(?:index.php|)\\?(.*)'), 'type': 'sourceforge2'})
    assert remote.extractBugid("http://sourceforge.net/support/tracker.php?aid=3555888")['aid'] == "3555888"
